### Changelog

All notable changes to this project will be documented in this file

#### [1.3.0]

> 05 March 2020

- Add ability to use several binding maps in one container (util.createInjectionProxy)
- Add ability to set specific bindings to specific values without resolving (util.createBindResolveMap)
- Add ability to configure `methods` option which defines availability of methods `Module` and `SubModule`
- Add ability to set `globals` option for bindings which will be available in all submodules of a module
- Methods `Module` and `SubModule` no longer are passed by extending user bindings object, but as globals instead
- As a result of elimination of object extension `CreateSubModule` method is now somewhat less complex
- Add option `self` to specify binding for current level container in a module (not set by default)
- Option `binding` can be now set to disable by setting "falsey" value
- Updated README.md with examplex of new options and features
- Add tests to cover new options and features

#### [1.2.0]

> 04 March 2020

- Add `options` argument to `Module` function
- Add option `binding` to specify the name for the module container binding; default behavior (bind as `'container'` is unchanged)
- Fix: `SubModule` function is now defined and frozen in Module function and passed down the chain of dependency
- Add new example to README.md how to use `options.binding` in `Module`
- Add tests to cover new option

#### [1.1.1]

> 03 March 2020

- Fix a typo in README.md
- Add a new commentary to README.md about Module/SubModule
- Add a missing line about fix in previous release to CHANGELOG.md

#### [1.1.0]

> 03 March 2020

- Introduced Module and SubModule - as a fix for rules 3 and 4 with nested containers
- Added rule to linter: 'prefer-object-spread': 0 because 2017 doesn't support spread
- Fix: delete circularity detector when an error is caught
- Added comment about circularity
- Added CHANGELOG.md
- Updated README.md

#### [1.0.1]

> 02 March 2020

- Fixed typo in README.md
- Fixed example in README.md

#### [1.0.0]

> 02 March 2020

- Initial release
- Implemented Container and util
- Added tests
- Added LICENSE
- Added README.md
- Set up GitLab-CI