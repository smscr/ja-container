const MyContainer = require('./Container.js');

class Main {
    constructor(container) {
        this.container = container;
        this.someResult = null;
        this.someOtherResult = null;
    }

    doSomething() {
        const { SomeDependency } = this.container;
        this.someResult = SomeDependency.someAction();
    }

    doSomethingElse() {
        const { SomeOtherDependency } = this.container;
        this.someOtherResult = SomeOtherDependency.someAction();
    }

    getSomethingOuter() {
        const { SomeDependencyFromOuterWorld } = this.container;
        return SomeDependencyFromOuterWorld;
    }

    getComparisonOfAlternatives() {
        const { SomeMethod, SomeMethodAlternative } = this.container;
        return [
            {
                result1: SomeMethod(1, 2, 3),
                result2: SomeMethodAlternative(2, 3, 4),
            },

            {
                result1: SomeMethod(1, 3, 6),
                result2: SomeMethodAlternative(1, 3, 6),
            },
        ];
    }

    getWorkFromAnotherContainer() {
        const { DependencyUsingAnotherContainer } = this.container;
        return DependencyUsingAnotherContainer.value;
    }
}

module.exports = Main.bind(null, MyContainer);
