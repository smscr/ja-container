const { Module } = require('../../src/index.js');

const SomeDependency = require('./dependency/SomeOtherDependency.js');

const MyModule = Module({
    SomeDependency,
    SomeCustomResolver(container) {
        return {
            type: typeof container,
        };
    },
    SubContainerSubModule: ({ SubModule }) => SubModule({
        UseSomething({
            NameOfTheModule: {
                SomeDependency: dependency,
            },
            SomethingInAnotherContainer,
        }) {
            return {
                SomeDependency: dependency,
                SomethingInAnotherContainer,
            };
        },
        SomethingInAnotherContainer: 123,
    }),
    SubContainerModule: ({ Module: module }) => module({
        SomethingInSubContainerModule: 234,
    }, {
        binding: false,
        methods: { Module: false, SubModule: false },
    }),
}, {
    binding: 'NameOfTheModule',
    self: 'NameOfTheCurrentLevel',
    globals: {
        Test: 567,
        Fun: ({ Test }, arg2) => Test + arg2,
        Broken: ({ SomeDependency: x }) => x,
        GlobalSub: ({ SubModule }) => SubModule({ TestSub: { a: 789 } }),
    },
});

module.exports = MyModule;
