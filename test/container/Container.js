const SomeDependencyFromOuterWorld = require('events').EventEmitter;

const { Container } = require('../../src/index.js');

const SomeDependency = require('./dependency/SomeDependency.js');
const SomeSubDependency = require('./dependency/SomeSubDependency.js');
const SomeOtherDependency = require('./dependency/SomeOtherDependency.js');
const DependencyUsingAnotherContainer = require('./dependency/DependencyUsingAnotherContainer.js');
const SomeMethod = require('./util/SomeMethod.js');

const MyContainer = Container({
    SomeDependency, // Error: Circularity of dependencies detected when resolving SomeDependency
    SomeSubDependency, // too
    SomeOtherDependency,
    SomeDependencyFromOuterWorld: () => new SomeDependencyFromOuterWorld(),
    SomeCustomResolver(container) {
        return {
            type: typeof container,
        };
    },
    SomeMethod: (container) => SomeMethod.bind(null, container),
    SomeMethodAlternative: SomeMethod,
    DependencyUsingAnotherContainer,
    AnotherContainer({ SomeOtherDependency: someDependencyInstance }) {
        const AnotherContainer = Container({
            AlternativeNameInAnotherContainer: someDependencyInstance,
            UseSomethingUniqueFromAnotherContainer({ SomethingUniqueToAnotherContainer }) {
                return { value: SomethingUniqueToAnotherContainer };
            },
            SomethingUniqueToAnotherContainer: 123,
        });

        return AnotherContainer;
    },
});

module.exports = MyContainer;
