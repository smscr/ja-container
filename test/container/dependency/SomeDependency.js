class SomeDependency {
    constructor({ SomeSubDependency }) {
        this.subDependency = SomeSubDependency;
    }

    someAction() {
        if (this.subDependency) {
            return true;
        }

        return false;
    }
}

module.exports = SomeDependency;
