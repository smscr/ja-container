class SomeSubDependency {
    constructor({ SomeDependency }) {
        this.dependency = SomeDependency;
    }
}

module.exports = SomeSubDependency;
