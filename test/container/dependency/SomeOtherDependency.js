class SomeOtherDependency {
    constructor({ SomeCustomResolver }) {
        this.typeOfContainer = SomeCustomResolver.type;
    }

    someAction() {
        return this.typeOfContainer;
    }
}

module.exports = SomeOtherDependency;
