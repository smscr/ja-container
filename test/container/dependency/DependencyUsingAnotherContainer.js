class DependencyUsingAnotherContainer {
    constructor({
        AnotherContainer: {
            AlternativeNameInAnotherContainer,
            UseSomethingUniqueFromAnotherContainer,
        },
    }) {
        this.dependency = AlternativeNameInAnotherContainer;
        this.value = UseSomethingUniqueFromAnotherContainer.value;
    }
}

module.exports = DependencyUsingAnotherContainer;
