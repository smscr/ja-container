const { Module } = require('../../src/index.js');

const SomeDependency = require('./dependency/SomeOtherDependency.js');

const MyModule = Module({
    SomeDependency,
    SomeCustomResolver(container) {
        return {
            type: typeof container,
        };
    },
    SubContainerSubModule: ({ SubModule }) => SubModule({
        UseSomething({
            container: {
                SomeDependency: dependency,
            },
            SomethingInAnotherContainer,
        }) {
            return {
                SomeDependency: dependency,
                SomethingInAnotherContainer,
            };
        },
        SomethingInAnotherContainer: 123,
    }),
    SubContainerModule: ({ Module: module }) => module({
        SomethingInSubContainerModule: 234,
    }),
});

module.exports = MyModule;
