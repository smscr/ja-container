function SomeMethod(container, arg1, arg2, arg3) {
    if (!container) {
        throw new Error('I desperately need container!');
    }

    return arg1 + arg2 + arg3;
}

module.exports = SomeMethod;
