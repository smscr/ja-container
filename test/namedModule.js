const assert = require('assert');

const MyNamedModule = require('./container/NamedModule.js');

function TestNamedModule() {
    describe('Test example of usage of Module with custom binding name', function () {
        it('Resolves plain depdenency', function () {
            assert.equal(typeof MyNamedModule.SomeDependency, 'object');
            assert.equal(MyNamedModule.SomeDependency.someAction(), 'object');
        });

        it('Resolves its own module container', function () {
            assert.equal(typeof MyNamedModule.NameOfTheModule, 'object');
            assert.equal(MyNamedModule.NameOfTheModule, MyNamedModule);
        });

        it('Resolves its own current level container', function () {
            assert.equal(typeof MyNamedModule.NameOfTheCurrentLevel, 'object');
            assert.equal(MyNamedModule.NameOfTheCurrentLevel, MyNamedModule);
        });

        it('Resolves a global binding in top container', function () {
            assert.equal(MyNamedModule.Test, 567);
            assert.equal(typeof MyNamedModule.Fun, 'function');
            assert.equal(MyNamedModule.Fun(1), 568);
        });

        it('Resolves a global SubModule in top container', function () {
            assert.equal(typeof MyNamedModule.GlobalSub.TestSub, 'object');
            assert.equal(MyNamedModule.GlobalSub.TestSub.a, 789);
        });

        it("Doesn't resolve module container bindings in global bindings", function () {
            assert.throws(() => {
                const { Broken } = MyNamedModule.Broken;
                assert.notStrictEqual(Broken, MyNamedModule.Broken);
            }, Error, 'Error: No such binding: SomeDependency');
        });

        it('Resolves a SubModule', function () {
            assert.equal(typeof MyNamedModule.SubContainerSubModule, 'object');
            assert.notStrictEqual(MyNamedModule.SubContainerSubModule, MyNamedModule);
        });

        it('Resolves a SubModule Dependencies', function () {
            const subDependency = MyNamedModule.SubContainerSubModule.UseSomething;
            assert.equal(subDependency.SomeDependency, MyNamedModule.SomeDependency);
            assert.equal(subDependency.SomethingInAnotherContainer, 123);
        });

        it('Resolves a global binding in SubModule container', function () {
            assert.equal(MyNamedModule.SubContainerSubModule.Test, 567);
            assert.equal(typeof MyNamedModule.SubContainerSubModule.Fun, 'function');
            assert.equal(MyNamedModule.SubContainerSubModule.Fun(1), 568);
        });

        it('Resolves a global SubModule in top container SubModule', function () {
            const globSubInSub = MyNamedModule.SubContainerSubModule.GlobalSub;
            assert.equal(typeof globSubInSub.TestSub, 'object');
            assert.equal(globSubInSub.TestSub.a, 789);
        });

        it('A global SubModule in top container SubModule is the same in top', function () {
            const testGlobSubInTop = MyNamedModule.GlobalSub.TestSub;
            const testGlobSubInSub = MyNamedModule.SubContainerSubModule.GlobalSub.TestSub;
            assert.equal(testGlobSubInTop, testGlobSubInSub);
            testGlobSubInTop.a += 10;
            assert.equal(testGlobSubInSub.a, 799);
            testGlobSubInSub.a -= 10;
            assert.equal(testGlobSubInTop.a, 789);
        });

        it('Resolves as SubModule current level container', function () {
            const SubCurrentLevel = MyNamedModule.SubContainerSubModule.NameOfTheCurrentLevel;
            assert.equal(typeof SubCurrentLevel, 'object');
            assert.equal(SubCurrentLevel, MyNamedModule.SubContainerSubModule);
        });

        it("Submodule container is parent Module's container", function () {
            const ModuleLevel = MyNamedModule.SubContainerSubModule.NameOfTheModule;
            assert.equal(ModuleLevel, MyNamedModule);
        });

        it("Submodule current level container is not parent Module's container", function () {
            const SubCurrentLevel = MyNamedModule.SubContainerSubModule.NameOfTheCurrentLevel;
            assert.notStrictEqual(SubCurrentLevel, MyNamedModule);
        });

        it('Resolves a nested Module', function () {
            assert.equal(typeof MyNamedModule.SubContainerModule, 'object');
            assert.notStrictEqual(MyNamedModule.SubContainerModule, MyNamedModule);
        });

        it('Resolves a nested Module Dependencies', function () {
            const subDependency = MyNamedModule.SubContainerModule.SomethingInSubContainerModule;
            assert.equal(subDependency, 234);
        });

        it("Nested Module container is NOT parent Module's container", function () {
            assert.throws(() => {
                const { NameOfTheModule: container } = MyNamedModule.SubContainerModule;
                assert.notStrictEqual(MyNamedModule, container);
            }, Error, 'Error: No such binding: NameOfTheModule');
        });

        it('In nested Module, methods Module and SubModule are disabled', function () {
            assert.throws(() => {
                const { Module } = MyNamedModule.SubContainerModule;
                assert.notStrictEqual(Module, MyNamedModule.Module);
            }, Error, 'Error: No such binding: Module');

            assert.throws(() => {
                const { SubModule } = MyNamedModule.SubContainerModule;
                assert.notStrictEqual(SubModule, MyNamedModule.SubModule);
            }, Error, 'Error: No such binding: SubModule');
        });
    });
}

module.exports = TestNamedModule;
