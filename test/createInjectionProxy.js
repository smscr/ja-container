const assert = require('assert');

const { createInjectionProxy } = require('../src/util.js');

const PlainClassEmpty = require('./classes/PlainClassEmpty.js');
const PlainClassWithValueAndArgs = require('./classes/PlainClassWithValueAndArgs.js');

function TestCreateInjectionProxy() {
    const proxySource = {
        SymbolState: Symbol('SymbolState'),
        NumberState: 100,
        StringState: 'valuable_state',
        FunctionState(arg1) {
            return arg1(100);
        },
        ClassState: PlainClassWithValueAndArgs,
        ObjectState: { x: 100 },
        InstanceState: new PlainClassEmpty(),
    };
    const proxyMap = new Map(Object.entries(proxySource));

    const injectionProxy = createInjectionProxy(proxyMap);

    describe('Dependency Injection with createInjectionProxy', function () {
        describe('Test inspection of injection proxy', function () {
            it("Doesn't have any iterable properties", function () {
                for (const test in injectionProxy) {
                    assert.fail(`Found property '${test}' in injection proxy using for ... in`);
                }
            });

            it("Doesn't have any keys", function () {
                assert.equal(Object.keys(injectionProxy).length, 0);
            });

            it("Doesn't have any values", function () {
                assert.equal(Object.values(injectionProxy).length, 0);
            });

            it("Doesn't have any own property names", function () {
                assert.equal(Object.getOwnPropertyNames(injectionProxy).length, 0);
            });

            it("Doesn't have any own property symbols", function () {
                assert.equal(Object.getOwnPropertySymbols(injectionProxy).length, 0);
            });
        });

        describe('Test modification of injection proxy', function () {
            it("Doesn't save any modified properties", function () {
                assert.throws(() => injectionProxy.test, Error, 'No such binding: test');
                injectionProxy.test = 123;
                assert.throws(() => injectionProxy.test, Error, 'No such binding: test');
            });

            it("Doesn't delete any properties which are in proxyMap", function () {
                const currentValue = injectionProxy.SymbolState;
                assert.notStrictEqual(currentValue, undefined);
                delete injectionProxy.SymbolState;
                assert.equal(injectionProxy.SymbolState, currentValue);
            });

            it('Object.defineProperty/Object.defineProperties produces TypeError', function () {
                assert.throws(() => {
                    Object.defineProperty(injectionProxy, 'a', {
                        value: 123,
                        writable: true,
                        enumerable: true,
                        configurable: true,
                    });
                }, TypeError, 'TypeError: Cannot define property a, object is not extensible');

                assert.throws(() => {
                    Object.defineProperties(injectionProxy, {
                        a: {
                            value: 123,
                            writable: true,
                            enumerable: true,
                            configurable: true,
                        },
                    });
                }, TypeError, 'TypeError: Cannot define property a, object is not extensible');
            });
        });

        describe('Test resolving of values with injection proxy', function () {
            it("Doesn't resolve values it doesn't have", function () {
                assert.throws(() => injectionProxy.Abcdef, Error, 'No such binding: Abcdef');
                assert.throws(() => injectionProxy.Qwerty, Error, 'No such binding: Qwerty');
            });

            it('Resolves a Symbol', function () {
                assert.equal(injectionProxy.SymbolState, proxySource.SymbolState);
            });

            it('Resolves a Number', function () {
                assert.equal(injectionProxy.NumberState, proxySource.NumberState);
            });

            it('Resolves a String', function () {
                assert.equal(injectionProxy.StringState, proxySource.StringState);
            });

            it('Resolves a Function', function () {
                assert.equal(injectionProxy.FunctionState, proxySource.FunctionState);
            });

            it('Resolves a class constructor', function () {
                assert.equal(injectionProxy.ClassState, proxySource.ClassState);
            });

            it('Resolves an Object', function () {
                assert.equal(injectionProxy.ObjectState, proxySource.ObjectState);
            });

            it('Resolves an instance of a class', function () {
                assert.equal(injectionProxy.InstanceState, proxySource.InstanceState);
            });
        });
    });
}

module.exports = TestCreateInjectionProxy;
