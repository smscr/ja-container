const assert = require('assert');

const MyModule = require('./container/Module.js');

function TestModule() {
    describe('Test example of usage of Module', function () {
        it('Resolves plain depdenency', function () {
            assert.equal(typeof MyModule.SomeDependency, 'object');
            assert.equal(MyModule.SomeDependency.someAction(), 'object');
        });

        it('Resolves its own container', function () {
            assert.equal(typeof MyModule.container, 'object');
            assert.equal(MyModule.container, MyModule);
        });

        it('Resolves a SubModule', function () {
            assert.equal(typeof MyModule.SubContainerSubModule, 'object');
            assert.notStrictEqual(MyModule.SubContainerSubModule, MyModule);
        });

        it('Resolves a SubModule Dependencies', function () {
            const subDependency = MyModule.SubContainerSubModule.UseSomething;
            assert.equal(subDependency.SomeDependency, MyModule.SomeDependency);
            assert.equal(subDependency.SomethingInAnotherContainer, 123);
        });

        it("Submodule container is parent Module's container", function () {
            assert.equal(MyModule, MyModule.SubContainerSubModule.container);
        });

        it('Resolves a nested Module', function () {
            assert.equal(typeof MyModule.SubContainerModule, 'object');
            assert.notStrictEqual(MyModule.SubContainerModule, MyModule);
        });

        it('Resolves a nested Module Dependencies', function () {
            const subDependency = MyModule.SubContainerModule.SomethingInSubContainerModule;
            assert.equal(subDependency, 234);
        });

        it("Nested Module container is NOT parent Module's container", function () {
            assert.notStrictEqual(MyModule, MyModule.SubContainerModule.container);
        });
    });
}

module.exports = TestModule;
