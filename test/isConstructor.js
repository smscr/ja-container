const assert = require('assert');

const { isConstructor } = require('../src/util.js');

const PlainClassEmpty = require('./classes/PlainClassEmpty.js');

function TestIsConstructor() {
    let called1 = false;
    let called2 = false;

    const expectConstructors = [
        function () { called1 = true; },
        function named() { called1 = true; },
        Object,
        PlainClassEmpty,
        Error,
        Boolean,
        Function,
    ];

    function TestExpectedConstructors() {
        expectConstructors.forEach((Value) => {
            const descr = String(Value).replace(/\n/g, '') || `(empty ${typeof Value})`;
            it(`Case: ${descr} is a constructor`, function () {
                assert.ok(isConstructor(Value));
                assert.equal(called1, false);
                assert.notStrictEqual(new Value(), undefined);
                called1 = false;
            });
        });
    }

    const expectNotConstructors = [
        0,
        123,
        '',
        'string',
        false,
        true,
        null,
        undefined,
        () => { called2 = true; },
        {},
        [],
        Symbol,
        Symbol('Symbol'),
        new PlainClassEmpty(),
    ];

    function TestExpectedNotConstructors() {
        expectNotConstructors.forEach((Value) => {
            const descr = String(Value).replace(/\n/g, '') || `(empty ${typeof Value})`;
            it(`Case: ${descr} is not a constructor`, function () {
                assert.equal(isConstructor(Value), false);
                assert.equal(called2, false);
                assert.throws(() => new Value(), TypeError, 'TypeError: value is not a constructor');
                called2 = false;
            });
        });
    }

    describe('Test whether util.isConstructor detects cases properly', function () {
        describe('Test expected constructors', TestExpectedConstructors);
        describe('Test expected non-constructors', TestExpectedNotConstructors);
    });
}

module.exports = TestIsConstructor;
