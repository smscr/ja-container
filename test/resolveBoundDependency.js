const assert = require('assert');

const { resolveBoundDependency } = require('../src/util.js');

const PlainClassEmpty = require('./classes/PlainClassEmpty.js');
const PlainClassWithValueAndArgs = require('./classes/PlainClassWithValueAndArgs.js');

function TestResolveBoundDependency() {
    const NullState = null;
    const UndefinedState = undefined;
    const SymbolState = Symbol('SymbolState');
    const NumberState = 100;
    const StringState = 'valuable_state';
    const FunctionState = function (arg1) {
        return { arg1 };
    };
    const ClassState = PlainClassWithValueAndArgs;
    const ObjectState = { x: 100 };
    const InstanceState = new PlainClassEmpty();

    const bound = Symbol('Something Bound');

    describe('Instantiation with resolveBoundDependency', function () {
        it('Resolves a null', function () {
            const result = resolveBoundDependency(NullState, bound);
            assert.equal(result, NullState);
        });

        it('Resolves an undefined', function () {
            const result = resolveBoundDependency(UndefinedState, bound);
            assert.equal(result, UndefinedState);
        });

        it('Resolves a Symbol', function () {
            const result = resolveBoundDependency(SymbolState, bound);
            assert.equal(result, SymbolState);
        });

        it('Resolves a Number', function () {
            const result = resolveBoundDependency(NumberState, bound);
            assert.equal(result, NumberState);
        });

        it('Resolves a String', function () {
            const result = resolveBoundDependency(StringState, bound);
            assert.equal(result, StringState);
        });

        it('Resolves a Function', function () {
            const result = resolveBoundDependency(FunctionState, bound);
            assert.notStrictEqual(result, FunctionState);
            assert.equal(typeof result, 'object');
            assert.equal(result.arg1, bound);
        });

        it('Resolves a class constructor', function () {
            const result = resolveBoundDependency(ClassState, bound);
            assert.notStrictEqual(result, ClassState);
            assert.ok(result instanceof ClassState);
            assert.equal(result.value, bound);
        });

        it('Resolves an Object', function () {
            const result = resolveBoundDependency(ObjectState, bound);
            assert.equal(result, ObjectState);
        });

        it('Resolves an instance of a class', function () {
            const result = resolveBoundDependency(InstanceState, bound);
            assert.equal(result, InstanceState);
        });
    });
}

module.exports = TestResolveBoundDependency;
