const assert = require('assert');

const { createBindResolveMap } = require('../src/util.js');

const PlainClassEmpty = require('./classes/PlainClassEmpty.js');
const PlainClassWithValueAndArgs = require('./classes/PlainClassWithValueAndArgs.js');

function TestCreateBindResolveMap() {
    const TestInstantiation = function (checks, bindings, bindResolveMap, target) {
        it("Doesn't resolve bindings it shound't have", function () {
            assert.equal(bindResolveMap.has('Abcdef'), false);
            assert.equal(bindResolveMap.has('Qwerty'), false);
            assert.equal(bindResolveMap.get('Abcdef'), undefined);
            assert.equal(bindResolveMap.get('Qwerty'), undefined);
        });

        for (const name in bindings) {
            it(`Resolves ${name} binding`, function () {
                assert.equal(bindResolveMap.has(name), true);
                const result1 = bindResolveMap.get(name);
                checks[name](result1);
                const result2 = bindResolveMap.get(name);
                checks[name](result2);
                assert.equal(result2, result1);
            });
        }

        it('Resolves names which were set intentionally', function () {
            const TestNameTarget = bindResolveMap.get('test_name');
            assert.equal(TestNameTarget, target);

            const nameFor123 = bindResolveMap.get('123_name');
            assert.equal(nameFor123, 123);
        });

        it('Applies first arg of two-arg function', function () {
            const result = bindResolveMap.get('TwoArgsFunctionState');
            const functionResult1 = result('Arg2');
            const functionResult2 = result('Arg2');
            assert.notStrictEqual(functionResult1, undefined);
            assert.notStrictEqual(functionResult2, undefined);
            assert.notStrictEqual(functionResult1, functionResult2); // should be different objects
            assert.equal(functionResult1.arg1, target);
            assert.equal(functionResult2.arg1, target);
            assert.equal(functionResult1.arg2, 'Arg2');
            assert.equal(functionResult2.arg2, 'Arg2');
        });
    };

    function InstantiationTestCase(target) {
        const statsResolves = {
            NullResultState: 0,
            UndefinedResultState: 0,
            FunctionState: 0,
            TwoArgsFunctionState: 0,
        };
        const bindings = {
            NullState: null,
            UndefinedState: undefined,
            NullResultState: () => {
                statsResolves.NullResultState += 1;
                return null;
            },
            UndefinedResultState: () => {
                statsResolves.UndefinedResultState += 1;
                return undefined;
            },
            SymbolState: Symbol('SymbolState'),
            NumberState: 100,
            StringState: 'valuable_state',
            FunctionState(arg1) {
                statsResolves.FunctionState += 1;
                return { arg1 };
            },
            TwoArgsFunctionState(arg1, arg2) {
                statsResolves.TwoArgsFunctionState += 1;
                return { arg1, arg2 };
            },
            ClassState: PlainClassWithValueAndArgs,
            ObjectState: { x: 100 },
            InstanceState: new PlainClassEmpty(),
        };
        const checks = {
            NullState: (result) => assert.equal(result, null),
            UndefinedState: (result) => assert.equal(result, undefined),
            NullResultState: (result) => {
                assert.equal(result, null);
                assert.equal(statsResolves.NullResultState, 1);
            },
            UndefinedResultState: (result) => {
                assert.equal(result, undefined);
                assert.equal(statsResolves.UndefinedResultState, 1);
            },
            SymbolState: (result) => assert.equal(result, bindings.SymbolState),
            NumberState: (result) => assert.equal(result, 100),
            StringState: (result) => assert.equal(result, 'valuable_state'),
            FunctionState: (result) => {
                assert.notStrictEqual(result, undefined);
                assert.equal(result.arg1, target);
                assert.equal(statsResolves.FunctionState, 1);
            },
            TwoArgsFunctionState: (result) => {
                assert.notStrictEqual(result, undefined);
                assert.equal(statsResolves.TwoArgsFunctionState, 0);
                assert.equal(typeof result, 'function');
                assert.equal(result.length, 1);
            },
            ClassState: (result) => {
                assert.notStrictEqual(result, undefined);
                assert.equal(result instanceof bindings.ClassState, true);
            },
            ObjectState: (result) => assert.equal(result, bindings.ObjectState),
            InstanceState: (result) => assert.equal(result, bindings.InstanceState),
        };

        const bindResolveMap = createBindResolveMap(bindings);

        if (target) {
            bindResolveMap.bind(target);
        }

        bindResolveMap.set('test_name', target);
        bindResolveMap.set('123_name', 123);

        TestInstantiation(checks, bindings, bindResolveMap, target);
    }

    const TestWithoutBoundTarget = function () {
        InstantiationTestCase();
    };

    const BindTargetAndTestInstantiation = function () {
        InstantiationTestCase(Symbol('Target Symbol'));
    };

    describe('Instantiation with bind-resolve map', function () {
        describe('Target = undefined', TestWithoutBoundTarget);
        describe('Target = target', BindTargetAndTestInstantiation);
    });
}

module.exports = TestCreateBindResolveMap;
