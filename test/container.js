const assert = require('assert');

const { EventEmitter } = require('events');
const Main = require('./container/Main.js');

function TestContainer() {
    const instance = new Main();

    describe('Test example of usage of Container', function () {
        it("Doesn't resolve circular dependency, but detects it", function () {
            assert.equal(instance.someResult, null);
            assert.throws(() => {
                instance.doSomething();
            }, Error, 'Error: Circularity of dependencies detected when resolving SomeDependency');
            assert.equal(instance.someResult, null);
        });

        it('Resolves plain depdenency', function () {
            assert.equal(instance.someOtherResult, null);
            instance.doSomethingElse();
            assert.equal(instance.someOtherResult, typeof instance.container);
        });

        it('Resolves outer depdenency', function () {
            const emitter = instance.getSomethingOuter();
            assert.ok(emitter instanceof EventEmitter);
        });

        it('Resolves argumented depdencenies', function () {
            const [
                { result1: r11, result2: r12 },
                { result1: r21, result2: r22 },
            ] = instance.getComparisonOfAlternatives();
            assert.equal(typeof r11, 'number');
            assert.equal(typeof r12, 'number');
            assert.equal(typeof r21, 'number');
            assert.equal(typeof r22, 'number');
            assert.notStrictEqual(r11, r12);
            assert.equal(r21, r22);
        });

        it('Resolves something from nested Container', function () {
            const result = instance.getWorkFromAnotherContainer();
            assert.equal(result, 123);
        });
    });
}

module.exports = TestContainer;
