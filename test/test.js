const TestIsConstructor = require('./isConstructor.js');
const TestResolveBoundDependency = require('./resolveBoundDependency.js');
const TestCreateInjectionProxy = require('./createInjectionProxy.js');
const TestCreateBindResolveMap = require('./createBindResolveMap.js');
const TestContainer = require('./container.js');
const TestModule = require('./module.js');
const TestNamedModule = require('./namedModule.js');

TestIsConstructor();
TestResolveBoundDependency();
TestCreateInjectionProxy();
TestCreateBindResolveMap();
TestContainer();
TestModule();
TestNamedModule();
