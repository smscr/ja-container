class PlainClassWithValueAndArgs {
    constructor(value, ...args) {
        this.value = value;
        this.args = args;
    }
}

module.exports = PlainClassWithValueAndArgs;
