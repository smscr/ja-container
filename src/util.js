const deeplyEmptyObj = Object.freeze(Object.create(null));
const constructCheck = { construct: () => ({}) };
const circularityDetector = Symbol('CircularityDetector');

function tryConstructFake(o) {
    return new (new Proxy(o, constructCheck))();
}

function isConstructor(o) {
    if (o === Symbol) return false;

    try {
        tryConstructFake(o);
        return true;
    } catch (e) {
        return false;
    }
}

function resolveBoundDependency(Resolver, target) {
    if (Resolver instanceof Function) {
        if (Resolver.length > 1) {
            return Resolver.bind(null, target);
        }

        if (isConstructor(Resolver)) {
            return new Resolver(target);
        }

        return Resolver(target);
    }

    return Resolver;
}

function createBindResolveMap(bindings) {
    let target;
    const bindingsMap = new Map(Object.entries(bindings));
    const resolveMap = new Map();
    const has = bindingsMap.has.bind(bindingsMap);

    function bind(value) {
        target = value;
        resolveMap.clear();
    }

    function set(name, value) {
        bindingsMap.set(name, value);
        resolveMap.set(name, value);
    }

    function get(name) {
        if (!has(name)) {
            return undefined;
        }

        if (resolveMap.has(name)) {
            const resolved = resolveMap.get(name);

            if (resolved === circularityDetector) {
                // Don't workaround circularity - for simplicity and consistency reasons
                throw new Error(`Circularity of dependencies detected when resolving ${name}`);
            }

            return resolved;
        }

        resolveMap.set(name, circularityDetector);
        const resolver = bindingsMap.get(name);
        try {
            const resolved = resolveBoundDependency(resolver, target);
            resolveMap.set(name, resolved);
            return resolved;
        } catch (e) {
            // delete circularity detector
            resolveMap.delete(name);
            throw e;
        }
    }

    return {
        has, get, set, bind,
    };
}

function createInjectionProxy(...proxyMaps) {
    // deeplyEmptyObj is used as it's unmodifiable and its the same for all proxies
    // and it makes you actually declare what you want to get, not peek at what you can get
    return new Proxy(deeplyEmptyObj, {
        get(_, name) {
            for (const proxyMap of proxyMaps) {
                if (proxyMap.has(name)) {
                    return proxyMap.get(name);
                }
            }

            throw new Error(`No such binding: ${String(name)}`);
        },
    });
}

module.exports = {
    isConstructor,
    resolveBoundDependency,
    createBindResolveMap,
    createInjectionProxy,
};
