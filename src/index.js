const util = require('./util.js');

const globalsMapOption = Symbol('Globals map');

function getModuleOptions(userOptions) {
    const moduleOptions = Object.assign({ binding: 'container', self: null }, userOptions);
    moduleOptions.methods = Object.assign({ Module: true, SubModule: true }, userOptions.methods);
    moduleOptions.globals = Object.assign({}, userOptions.globals);
    return moduleOptions;
}

// Container that only passes what explicitly specified
function Container(bindings) {
    const bindResolveMap = util.createBindResolveMap(bindings);
    const container = util.createInjectionProxy(bindResolveMap);
    bindResolveMap.bind(container);
    return container;
}

// SubModule is a Container that passes parent Container into the child one to simplify
// passing bindings into child Container intended for structuring, not isolation
function CreateSubModule(moduleOptions, _, bindings) {
    const { self, [globalsMapOption]: globalsMap } = moduleOptions;

    const subModuleMap = util.createBindResolveMap(bindings);
    const subModuleContainer = util.createInjectionProxy(subModuleMap, globalsMap);
    subModuleMap.bind(subModuleContainer);

    if (self) {
        // Set binding of the container to itself if the name for such binding provided
        subModuleMap.set(self, subModuleContainer);
    }

    return subModuleContainer;
}

// Module is a Container that passes itself into its contents to simplify
// retrieval of container and bindings at the same time
const Module = Object.freeze((_, bindings, options = {}) => {
    const moduleOptions = getModuleOptions(options);
    const SubModule = Object.freeze(CreateSubModule.bind(null, moduleOptions));
    const {
        self, binding, methods, globals,
    } = moduleOptions;

    // If methods were not disabled, add methods Module and SubModule as pre-defined globals
    if (methods.Module) globals.Module = Module;
    if (methods.SubModule) globals.SubModule = SubModule;

    // Globals have their separate container for resolving
    const globalsMap = util.createBindResolveMap(globals);
    const globalsContainer = util.createInjectionProxy(globalsMap);
    globalsMap.bind(globalsContainer);

    // Create module bindings container which includes globals map with lesser priority
    const moduleMap = util.createBindResolveMap(bindings);
    const moduleContainer = util.createInjectionProxy(moduleMap, globalsMap);
    moduleMap.bind(moduleContainer);

    if (binding) {
        // Set module container to be bound into global map as [binding] if not disabled
        globalsMap.set(binding, moduleContainer);
    }

    if (self) {
        // Set binding of the container to itself if the name for such binding provided
        moduleMap.set(self, moduleContainer);
    }

    // Set globalsMap into options to be used by SubModule
    moduleOptions[globalsMapOption] = globalsMap;
    return moduleContainer;
});

const boundModule = Module.bind(null, null);

module.exports = { util, Container, Module: boundModule };
