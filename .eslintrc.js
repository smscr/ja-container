module.exports = {
    env: {
        browser: true,
        commonjs: true,
        es6: true,
        node: true,
        mocha: true,
    },
    plugins: [
        'mocha',
    ],
    extends: [
        'airbnb-base',
        'plugin:mocha/recommended',
    ],
    globals: {
        Atomics: 'readonly',
        SharedArrayBuffer: 'readonly',
    },
    parserOptions: {
        ecmaVersion: 2017,
    },
    rules: {
        'func-names': 0,
        'prefer-arrow-callback': 0,
        'mocha/prefer-arrow-callback': 2,
        'indent': ['error', 4],
        'guard-for-in': 0,
        'prefer-object-spread': 0,
        'no-restricted-syntax': [
            'error',
            'LabeledStatement',
            'WithStatement',
        ],
    },
};
